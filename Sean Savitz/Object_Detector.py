## Object Detection Using TensorFlow and Raspberry Pi Camera ##
 
# Name: Sean Savitz
# Student Number: c1673010
# Submission Date: 15/5/2020


# The neural network models that we have used are found at TensorFlow's Model Zoo at
# https://github.com/tensorflow/models/blob/master/research/object_detection/g3doc/detection_model_zoo.md


# This code is implemented using my ideas with ideas from TensorFlow's Object Detector at
# https://github.com/tensorflow/models/blob/master/research/object_detection/object_detection_tutorial.ipynb
# And ideas from a modification of the code at
# https://github.com/EdjeElectronics/TensorFlow-Object-Detection-on-the-Raspberry-Pi/blob/master/Object_detection_picamera.py


# Code that I wrote solely is cleared marked out.

# Import packages for Object detection

import os
import cv2
import numpy as np
from picamera.array import PiRGBArray
from picamera import PiCamera
import tensorflow as tf
import argparse
import sys

#Import packages for light sensor

import time
import grovepi


#Import packages for ThingsBoard.io - IoT Cloud Platform

import paho.mqtt.client as mqtt
import time,json

# Code for Object Detection:

# Set up camera constants. This can be mofified depending on resolution requirements.
IM_WIDTH = 704    
IM_HEIGHT = 464

# Camera used is the picamera
camera_type = 'picamera'

sys.path.append('..')

# Import utilites
from utils import label_map_util
from utils import visualization_utils as vis_util

# The neural network model that we can currently using. Change this to the other directories.
MODEL_NAME = 'ssd_mobilenet_v1_coco_2018_01_28'

# Grab path to current working directory
CWD_PATH = os.getcwd()

# The 'frozen_inference_graph.pb' contains the model used for object detection
PATH_TO_CKPT = os.path.join(CWD_PATH,MODEL_NAME,'frozen_inference_graph.pb')

# This contains the list of integers that are maped to the name of the objects in the MS COCO dataset
PATH_TO_LABELS = os.path.join(CWD_PATH,'data','mscoco_label_map.pbtxt')

# Number of classes the object detector can identify
NUM_CLASSES = 90

# maps integers to category names e.g. 2 is a bicycle
label_map = label_map_util.load_labelmap(PATH_TO_LABELS)
categories = label_map_util.convert_label_map_to_categories(label_map, max_num_classes=NUM_CLASSES, use_display_name=True)
category_index = label_map_util.create_category_index(categories)

# Load the Tensorflow model into memory.
detection_graph = tf.Graph()
with detection_graph.as_default():
    od_graph_def = tf.GraphDef()
    with tf.gfile.GFile(PATH_TO_CKPT, 'rb') as fid:
        serialized_graph = fid.read()
        od_graph_def.ParseFromString(serialized_graph)
        tf.import_graph_def(od_graph_def, name='')

    sess = tf.Session(graph=detection_graph)


# Define input and output tensors (i.e. data) for the object detection classifier

# Input tensor is the image
image_tensor = detection_graph.get_tensor_by_name('image_tensor:0')

# Output tensors are the detection boxes, scores, and classes
# Each box represents a part of the image where a particular object was detected
detection_boxes = detection_graph.get_tensor_by_name('detection_boxes:0')

# Each score represents level of confidence for each of the objects.
# The score is shown on the result image, together with the class label.
detection_scores = detection_graph.get_tensor_by_name('detection_scores:0')
detection_classes = detection_graph.get_tensor_by_name('detection_classes:0')

# This is the number of objects detected
num_detections = detection_graph.get_tensor_by_name('num_detections:0')


# frame rate calculation is initialised
frame_rate_calc = 1
freq = cv2.getTickFrequency()
font = cv2.FONT_HERSHEY_SIMPLEX

# --------------------------
## Code written by Sean Savitz

# Configuration for ThingsBoard. 
def on_log(client, userdata, level, buf):
   print(buf) 
def on_connect(client, userdata, flags, rc):
    if rc==0:
        client.connected_flag=True 
        print("connected OK")
    else:
        print("Bad connection Returned code=",rc)
        client.loop_stop()  
def on_disconnect(client, userdata, rc):
   print("client disconnected ok")
def on_publish(client, userdata, mid):
    print("In on_pub callback mid= "  ,mid)
count=0
mqtt.Client.connected_flag=False
mqtt.Client.suppress_puback_flag=False
client = mqtt.Client("python1")             

client.on_connect = on_connect
client.on_disconnect = on_disconnect
client.on_publish = on_publish

broker="demo.thingsboard.io"
port =1883
topic="v1/devices/me/telemetry"
# change username to the access token of the device in ThingsBoard
username="ZjRxeitwFb3AQJlkXLf9"
password=""
if username !="":
   pass
client.username_pw_set(username, password)
# establish connection
client.connect(broker,port)           
while not client.connected_flag: 
   client.loop()
   time.sleep(1)
time.sleep(3)
data = {}


# The reconfigurable for different purposes feature
# User chooses which object they want to detect make it object-specific
print("This is an Object-Detection IoT Solution")
print("Type in the Number Representing each Class of Objects that you Would like to Detect")
print("Bicycle: 1, Car/ Truck/ Bus: 2, Mobile Phone: 3")
object_chosen = input()

Bicycle = 2
Car = 3
Truck = 8 
Bus = 6
Cell_phone = 77

# This maps the integer relating to the object to the name of the object

if object_chosen == "1":
    object_class = Bicycle
if object_chosen == "2":
    object_class = Car or Truck or Bus
if object_chosen == "3":
    object_class = Cell_phone

#-------------------------------


# Initialise the use of the pi camera. We use Pi CameraV2 in our investigations

camera = PiCamera()
camera.resolution = (IM_WIDTH,IM_HEIGHT)
camera.framerate = 10
rawCapture = PiRGBArray(camera, size=(IM_WIDTH,IM_HEIGHT))
rawCapture.truncate(0)

for frame1 in camera.capture_continuous(rawCapture, format="bgr",use_video_port=True):

    t1 = cv2.getTickCount()
    
    # Acquire frame and expand frame dimensions to have shape: [1, None, None, 3]
    # i.e. a single-column array, where each item in the column has the pixel RGB value
    frame = np.copy(frame1.array)
    frame.setflags(write=1)
    frame_rgb = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
    frame_expanded = np.expand_dims(frame_rgb, axis=0)

    # Perform the actual detection by running the model with the image as input
    (boxes, scores, classes, num) = sess.run(
        [detection_boxes, detection_scores, detection_classes, num_detections],
        feed_dict={image_tensor: frame_expanded})
    
# ---------------------------
## Code written by Sean Savitz

# If the highest probability score recorded related to our object that we have chosen:
    if int(classes[0][0]) == int(object_class):
        # Identify light sensor readings from GrovePi+
        light_sensor = 0
        grovepi.pinMode(light_sensor,"INPUT")
        light_sensor_value = grovepi.analogRead(light_sensor)
        
        # This is the Resource Monitoring Agent
        # Collection storage of key measurements from system
        # Temperature data     
        temperature_data = os.popen('vcgencmd measure_temp').read()
        current_temp = (round(int((temperature_data[5:7]))))
        # RAM memory usage data 
        free_memory_info = os.popen('free -m').read()
        memory_stats = free_memory_info.splitlines()[1]
        # CPU load data
        uptime = os.popen('uptime').read()
        current_CPU_load=uptime.splitlines()[0]
        #CPU usage data
        mpstat = os.popen('mpstat').read()
        current_CPU_usage = mpstat.splitlines()[3]
        
        resource_monitoring_agent = {"RAM_used": memory_stats.split()[2], "CPU_load":current_CPU_load.split()[8].replace(',',''),
        "CPU_usage":current_CPU_usage.split()[2] ,"Temperature": current_temp}
         
        
        for category in categories:
            if category["id"] == object_class:
                name_of_object = category["name"]
        
        detected_object = {"object":name_of_object,"probability_score":str(scores[0][0])}
        data = {"Performance": frame_rate_calc, detected_object["object"]: detected_object["probability_score"], "light_value": light_sensor_value, "RAM used":resource_monitoring_agent["RAM_used"],
                "CPU load":resource_monitoring_agent["CPU_load"],  "CPU used": resource_monitoring_agent["CPU_usage"],  "Temperature": resource_monitoring_agent["Temperature"]}
        # Send the dictionary with all data to the cloud Platform, ThingsBoard
        data_out=json.dumps(data) #create JSON object
        print("publish topic",topic, "data out= ",data_out)
        ret=client.publish(topic,data_out,0)    #publish
        client.loop()
        
# --------------------------
        
    # This helps to visualise the results. Change configuration to change layout.
    vis_util.visualize_boxes_and_labels_on_image_array(
        frame,
        np.squeeze(boxes),
        np.squeeze(classes).astype(np.int32),
        np.squeeze(scores),
        category_index,
        use_normalized_coordinates=True,
        line_thickness=8,
        min_score_thresh=0.40)

    cv2.putText(frame,"FPS: {0:.2f}".format(frame_rate_calc),(30,50),font,1,(255,255,0),2,cv2.LINE_AA)

    # All the results have been drawn on the frame, so it's time to display it.
    cv2.imshow('Object detector', frame)

    t2 = cv2.getTickCount()
    time1 = (t2-t1)/freq
    frame_rate_calc = 1/time1

    # Press 'q' to quit
    if cv2.waitKey(1) == ord('q'):
        break

    rawCapture.truncate(0)

camera.close()

cv2.destroyAllWindows()



